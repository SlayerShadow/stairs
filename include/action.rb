class Action
	@@locales = {}

	class << self
		attr_accessor :instance

		def logger
			@@logger ||= Stairs.logger
		end
	end

	def logger
		@@logger ||= Stairs.logger
	end

	def initialize
		self.class.instance = self
	end

	def text_to client, symbol, scope = @@locales[ self.class ]
		client.message :system, :message, { message: I18n.t( symbol, scope: scope, locale: client.locale ) }
		false
	end
end
