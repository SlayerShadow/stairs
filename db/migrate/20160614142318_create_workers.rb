class CreateWorkers < ActiveRecord::Migration[5.1]
	def change
		create_table :workers do |t|
			t.string :email,                       null: false
			t.string :password_digest, limit: 128, null: false
			t.string :auth_token,      limit: 32,  null: false
			t.string :name,            limit: 128, null: false

			t.timestamps null: false

			t.index :email,              unique: true
			t.index :auth_token,         unique: true
		end
	end
end
