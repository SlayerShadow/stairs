require_relative 'main_config'

module Stairs::AppModule

	ActionSpace = {}

	Dir[ Stairs.root.join 'include/**/*.rb' ].sort_by{ |e| e.scan( /\\|\// ).count }.each{ |rb_file| require_relative rb_file }

	require_relative Stairs.root.join( 'app/action' )

	Dir[ Stairs.root.join 'app/actions/**/*.rb' ].each{|action|
		const_name = action[ /(?<=actions[\\\/]).+?(?=\.rb)/ ]
		const_name.scan( /[\\\/]?(.+)/ ).each{|part|
			stringify_classes = part[ 0 ].gsub( /[\\\/]/, '::' ).gsub( /([^\:]+)/ ){ $1.capitalize }

			previous = []

			stringify_classes.split( '::' ).each{|name|
				previous << name
				eval <<-EOF
					class Action::#{ previous.join '::' } < Action
						@@locales[ self ] = to_s.downcase[ /(?<=\\Aaction\\:\\:).+/ ].gsub '::', '.'

						@@before_filters = []
						@@after_filters = []
						@@json_validators = []
						def self.before_filter name, options = {}
							@@before_filters << { action: name.to_s, on: [ options[ :on ] ].flatten.compact.map( &:to_s ), except: [ options[ :except ] ].flatten.compact.map( &:to_s ) }
						end
						def self.after_filter name, options = {}
							@@after_filters << { action: name.to_s, on: [ options[ :on ] ].flatten.compact.map( &:to_s ), except: [ options[ :except ] ].flatten.compact.map( &:to_s ) }
						end
						def self.validate_json name, failure_action = nil, schema
							@@json_validators << { action: name.to_s, failure: failure_action && failure_action.to_s, validator: -> ( json ) { JSON::Validator.validate! schema, json } }
						end
						def before_filters
							@@before_filters
						end
						def after_filters
							@@after_filters
						end
						def json_validators
							@@json_validators
						end
					end
				EOF
			}
		}

		require_relative action

		class_value = ::Action.const_get const_name.gsub( /[\\\/]/, '::' ).gsub( /([^\:\:]+)/ ){ $1.capitalize }
		ActionSpace[ const_name.gsub('/', '_') ] = class_value.new
	}

	ActionSpace.freeze

end

Dir[ Stairs.root.join 'app/models/*' ].each{ |file| require_relative file }
