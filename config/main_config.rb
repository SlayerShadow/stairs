module Stairs::AppModule

	class << self
		attr_reader :config
	end

	Stairs.root = Pathname.new( File.expand_path '../..', __FILE__ ).freeze
	@config = {}

	Stairs.env = ENV[ 'e' ] || ENV[ 'env' ] || ENV[ 'environment' ]
	Stairs.env ||= open( Stairs.root.join( '.env' ), 'rb', &:read )[ /(?<=environment\=)\w+/ ] if File.exists?( Stairs.root.join( '.env' ) )
	Stairs.env ||= 'development'

	@config.merge! Psych.load_file( Stairs.root.join 'config/config.yml' )[ Stairs.env ]
	@config.merge! db: Psych.load_file( Stairs.root.join 'config/database.yml' )[ Stairs.env ]

	symbolize_keys_deep = -> ( data ) {
		data.keys.each{|key|
			sym = key.to_sym
			data[ sym ] = data.delete key
			symbolize_keys_deep.call data[ sym ] if data[ sym ].kind_of? Hash
		}
	}
	symbolize_keys_deep.call @config

	@config[ :pidfile ] ||= Stairs.root.join( 'tmp/server.pid' ).to_s

	Stairs.logger = Stairs.env =~ /development|test/ ? ::Logger.new( STDOUT ) : ::Logger.new( @config[ :logger ], 10, 1024000 )
	Stairs.logger.level = Stairs.env != 'test' ? ::Logger::DEBUG : ::Logger::INFO

	ActiveRecord::Base.establish_connection config[ :db ]
	ActiveRecord::Base.logger = case Stairs.env
		when 'development' then Logger.new STDOUT
		when 'test' then nil
		else Logger.new config[ :logger ], 10, 1024000
	end

end
