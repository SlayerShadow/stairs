require 'bundler/setup'

Bundler.require :default

require 'logger'
require 'pathname'

require_relative 'lib/stairs'

module ApplicationName
	Stairs::AppModule = self
	Stairs.app_module = self
end

require_relative 'config/loader'

Bundler.require :development if Stairs.env != 'production'
Bundler.require :test if Stairs.env == 'test'

module Stairs::AppModule

	class Client < EM::Connection
		include EM::Protocols::MsgpackProtocol

		actions = Psych.load_file Stairs.root.join( 'config/routes.yml' )
		ReceivedActions = actions[ 'receiving_actions' ]
		SendActions = actions[ 'sending_actions' ]

		def self.logger
			@@logger ||= Stairs.logger
		end

		def logger
			@@logger ||= Stairs.logger
		end

		def post_init
			logger.debug :client_connected

			@unbind_callbacks = []
			@before_sending_data_changings = []
			@before_sending_filters = []
			@after_received_filters = []
			@after_sending_callbacks = []

			@attributes = {}
		end

		def receive_object object
			logger.debug received: object

			if ( route = ReceivedActions[ object[ 'service' ] ] ) && ( service = ActionSpace[ route[ 'module_name' ] ] )
				logger.debug route: route, service: [ service.class, service.object_id ]

				action = route[ 'actions' ][ object[ 'action' ] ]

				if @after_received_filters.all?{ |filter| filter.call self, object }
					validator_object = service.json_validators.find{ |validator| validator[ :action ] == action }

					logger.debug validator_object: validator_object

					if !validator_object || validator_object[ :validator ].call( object )
						filters = service.before_filters.select{ |filter| ( filter[ :on ].empty? || filter[ :on ].include?( action ) ) && ( filter[ :except ].empty? || !filter[ :except ].include?( action ) ) }
						if filters.all?{ |filter| service.send filter[ :action ], self, object }
							service.public_send action, self, object
							filters = service.after_filters.select{ |filter| filter[ :on ].include?( action ) && !filter[ :except ].include?( action ) }
							filters.each{ |filter| service.send filter[ :action ], self, object }
						else
							logger.debug 'Before_filters_failed'
						end
					else
						r = { validator_object: validator_object }
						r[ :validator ] = validator_object[ :validator ] if validator_object
						validator_object[ :failure ] && validator_object[ :failure ].call( self, object )
						logger.debug validator_failed: r
					end
				else
					logger.debug after_filters_failed: @after_received_filters.map{ |filter| filter.call self, object }
				end
			else
				logger.debug routing_failed: { route: route, service: service }
			end
		rescue => error
			logger.debug error_data: object
			logger.debug error
			$!.backtrace.each{ |string| logger.debug string }
			raise error if Stairs.env == 'test'
		end

		def message service, action, data = nil
			if ( route = SendActions[ service.to_s ] ) && ( action_value = route[ 'actions' ].index action.to_s )
				if @before_sending_filters.all?{ |filter| filter.call self, data }
					@before_sending_data_changings.each{ |action| data = action.call self, data }
					logger.debug sending_data: { route: route[ 'service_command' ], action_value: action_value, data: data }

					commands = { service: route[ 'service_command' ], action: action_value }

					send_object data ? data.merge( commands ) : commands
					@after_sending_callbacks.each{ |callback| callback.call self, sending }
				end
			else
				raise ArgumentError, "Route or action not defined (route: #{ route.inspect }, action: #{ action.inspect }, service: #{ service.inspect }, requested_action: #{ action_value.inspect })"
			end
		end

		def unbind_callback action
			@unbind_callbacks << action
		end

		def before_sending_filter action
			@before_send_filters << action
		end

		def after_received_filter action
			@after_received_filters << action
		end

		def after_sending_callback action
			@after_sending_callbacks << action
		end

		def before_sending_change_data action
			@before_sending_data_changings << action
		end

		def unbind
			@unbind_callbacks.each{ |action| action.call self }
			logger.debug disconnected: id
		end
	end

end


`mkdir -p #{ Stairs.root.join Stairs.app_module.config[ :pidfile ], '..' }` if Stairs.env !~ /development|test/

at_exit{ File.delete Stairs.app_module.config[ :pidfile ] if File.exists?( Stairs.app_module.config[ :pidfile ] ) && Process.pid == open( Stairs.app_module.config[ :pidfile ], 'rb', &:read ).to_i }
Process.kill 'QUIT', open( Stairs.app_module.config[ :pidfile ], 'rb', &:read ).to_i if File.exists? Stairs.app_module.config[ :pidfile ]

if $0 == __FILE__
	EM.run{
		host, port = [ :host, :port ].map{ |arg| Stairs.app_module.config[ arg ] }

		EM.start_server host, port, Stairs.app_module::Client
		open( Stairs.app_module.config[ :pidfile ], 'wb' ){ |file| file << Process.pid } if Stairs.env !~ /development|test/

		Stairs.logger.debug server_started: { host: host, port: port }
	}
end
