# Stairs

Stairs is framework for socket-based applications (in future).

## Major goals

- `stairs new app` will create a skeleton of application with minimum configuration required.
- `stairs.gemspec` for dependencies.
- `gem install stairs` for installation.
- `bundle exec stairs server` or `bundle exec ruby main.rb` will run an app.
- `bundle exec rspec` runs all specs (partially done, need async support).
- Exclude `ActiveRecord` from strong dependencies to support any other database engine (or without database when it needed).
- Exclide MessagePack protocol from defaults to feeling free for another technics.

## Getting started

1. Clone the repo to yourself:

       $ git clone git@bitbucket.org:SlayerShadow/stairs.git

2. Run bundler:

       $ bundle install

3. Configure `config/config.yml` and `config/database.yml` from samples.

4. Run app:

       $ bundle exec ruby main.rb

## Developing

### Application structure

- All user code collected at `/app` folder.
- `/config` folder contains configuration of whole source.
- `/db` folder contains migrations and sqlite3 databases.
- `/include` folder used for user modules. All content will be loaded.
- `/lib` folder used for temporarily required modules. Each file will be loaded after requiring it.
- `/log` used for logs.
- `/spec` contains tests.
- `/vendor` (in gitignore) used for distributed dependencies or for capistrano publishing.

#### /app

It contains `/app/actions` and `/app/models` folders.
`/app/actions` folder used for routing your socket requests.
`/app/models` folder used for ActiveRecord models.
All content loads at the start.

`/app/action.rb` file is a main template of actions. It is blank and it collects all user attributes/parameters/actions which will be used in all routes.

#### /config

Core of evil.

1. First, loads `/config/loader.rb`. It is load all other configs and dynamically generates all default classes and methods. Also requires all `/include` folder and `/config/main_config.rb` file.
2. `/config/main_config.rb` contains all configurations: environment, database, loggers etc.
3. `/config/config.yml` contains server configuration, now it is only `host` and `port`.
4. `/config/database.yml` contains database information.
5. `/config/routes.yml` contains all routing system.

#### /include

The only required file is `/include/action.rb` which contains a few helpers.

#### /lib

The only required files are `/lib/stairs.rb` and `/lib/tasks/database.rb`.
- `/lib/stairs.rb` is the core `Stairs` module initialization.
- `/lib/tasks/database.rb` is the rake task for database migrations.

#### /spec

- The main file is `/spec/spec_helper.rb`. It sets up all RSpec configuration after the main project file loading. Also it loads `/spec/support` and `/spec/factories` folders.
- `/spec/support/helpers.rb` included to specs, it contains helper methods for tests.
- `/spec/support/client.rb` - response extension for main socket client class.
- `/spec/support/response.rb` - responses helper for socket. When socket receives any object, this object is available by `#read_object` calling (in your test). Then this object removes, and the next action calling will return the next object, if available. Else it raises exception.

## Basic usage

1. `/app/actions/base.rb`:

~~~ruby
class Action::Base < Action
  def authorize client, data
    if success?
      client.message :system, :success, returned_user: data[ 'user' ]
    else
      client.message :system, :failure
    end
  end

  private

  def success?
    rand( 2 ) == 0
  end
end
~~~

2. For this to start working it need to add routes to `/config/routes.yml`:

        receiving_actions:
          -
            module_name: base
            actions:
              - authorize

        sending_actions:
          system:
            service_command: 0
            actions:
              - success
              - failure

3. Run it: `bundle exec ruby main.rb`

## API

- Request, converted by `MessagePack` to bytestream:

~~~ruby
{ service: 0, action: 0, user: 'Username' }
~~~

- Available responses:

~~~ruby
{ 'service' => 0, 'action' => 0, 'returned_user' => 'Username' }
{ 'service' => 0, 'action' => 1 }
~~~

  It needed to be decoded by `MessagePack`.

## Testing

`/spec/actions/base_spec.rb`:

~~~ruby
RSpec.describe Action::Base do
  describe '#authorize' do
    context 'succeeds' do
      before do
        allow( described_class.instance ).to receive( :success? ).and_return( true )
        send_object service: 0, action: 0
      end

      it 'with returned user' do
        expect( read_object ).to have_key( 'returned_user' )
      end

      it 'with service 0 and action 0' do
        expect( read_object[ 'service' ] ).to eq( 0 )
        expect( read_object[ 'action' ] ).to eq( 0 )
      end
    end

    context 'fails' do
      before do
        allow( described_class.instance ).to receive( :success? ).and_return( false )
        send_object service: 0, action: 0
      end

      it 'without user' do
        expect( read_object ).not_to have_key( 'returned_user' )
      end

      it 'with service 0 and action 1' do
        expect( read_object[ 'service' ] ).to eq( 0 )
        expect( read_object[ 'action' ] ).to eq( 1 )
      end
    end
  end
end
~~~

The examples should all pass.

## Advanced usage

### Filters and callbacks

#### Receiving filters

- After received filters: `after_received_filter( :action_method )`
- Before filters: `before_filter( :action_method )`
- Run service action
- After filters: `after_filter( :action_method )`

#### Sending filters/callbacks

- Before_sending_filters: `client.before_sending_filter method( :action_method )`
- Changing data: `client.before_sending_change_data method( :action_method )`
- Message sending
- After sending callbacks: `client.after_sending_callback method( :action_method )`

#### Other filters/callbacks

`client.unbind_callback method( :action_method )`. All these callbacks run at disconnecting the client.

#### Examples

Sending filters can receive `proc` objects instead of action methods. Receiving filters are not.

Treatment process will be stopped if any before filter failed.

Receiving filters are set in actions:

~~~ruby
class Action::Base < Action
  before_filter :check_email, on: :authorize

  private

  def check_email client, data
    data[ 'email' ] && data[ 'email' ] =~ /^[\w\._-]+@[\w\._-]+\.\w+$/
  end
end
~~~

Sending filters are set for the `client` object:

~~~ruby
class Action::Base < Action
  def authorize client, data
    if data[ 'email' ] && data[ 'password' ]
      client.before_sending_change_data method( :encrypt )
    end
  end

  private

  def encrypt client, sending_data
    if sending_data && sending_data[ :crypted ]
      sending_data[ :crypted ] = Base64.encode64 sending_data[ :crypted ]
    end
  end
end
~~~

`before_sending_change_data` and `after_sending_callbacks` are not filters and returning value has no effect. In other cases they are the same as `before_sending_filters`.

#### Arguments

- Action name (_required_) - filtered action
- Options (_optional_):
  - `{ on: :action }` - calls on one `action`
  - `{ on: [ :action1, :action2 ] }` - may be called on several actions
  - `{ except: :action_name }` - calls on all actions except `action_name`
  - `{ except: [ :action1, :action2 ] }`, same but for several actions
  - `{ on: [ :action1, :action2 ], except: [ :action2, :action3 ] }` - will be called for `action1` only, so `except` has higher priority.

### Validations

Thanks for [json-schema](https://github.com/ruby-json-schema/json-schema) gem.

~~~ruby
class Action::Base < Action
  validate_json :authorize, :auth_format_failed,
    type: :object,
    required: [ :email, :password ],
    properties: {
      email: { type: :string },
      password: { type: :string }
    }

  private

  def auth_format_failed client, data
    client.message :system, :invalid_json_format
  end
end
~~~

#### Arguments

- Action name (_required_) - action where validation must be called
- Failure action (_optional_) - will be called when validation fails
- Json schema (_required_) - see [json-schema](https://github.com/ruby-json-schema/json-schema) gem
