desc 'Migrate database'
task 'db:migrate' do
	ActiveRecord::Migrator.migrate( Stairs.root.join( 'db/migrate' ), ENV["VERSION"] ? ENV["VERSION"].to_i : nil )
end
