class Response
	def initialize
		@io = StringIO.new
		@byte_array = []
	end

	def write data
		@io.pos = @io.string.bytesize
		@io << MessagePack.pack( data )
		@byte_array << @io.pos - @byte_array.last.to_i
	end

	def read_object
		if @io.size > 0
			@io.pos = 0
			string = @io.read @byte_array.first

			@io.string = @io.string[ string.bytesize .. -1 ]
			@byte_array.shift

			MessagePack.unpack string
		else
			raise IOError, 'Empty data reading'
		end
	end

	def clear
		@io.string = ''
		@byte_array.clear
	end
end
