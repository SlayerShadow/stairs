class Stairs.app_module::Client < EM::Connection
	attr_reader :response

	def initialize
		@response ||= Response.new
	end

	def send_object data
		@response.write data
	end

	def close_connection
		unbind
	end
end
