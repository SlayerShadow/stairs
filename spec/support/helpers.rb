module ServerHelpers

	@@clients = []

	def client
		@client ||= build_client
	end

	def build_client
		object = Stairs.app_module::Client.new nil
		@@clients << object
		object
	end

	def send_object receiver = client, hash
		receiver.receive_object MessagePack.unpack( MessagePack.pack hash )
	end

	def read_object receiver = client
		receiver.response.read_object
	end

	def clear_clients
		@@clients.each{|object|
			object.close_connection
			object.response.clear
		}
		@@clients.clear
	end

	def clear_actions
		Action.constants.select{ |constant| Action.const_get( constant ).class == Class }.each{|constant|
			Action.const_get( constant ).instance_eval{
				constants.each{ |e| remove_const e if const_get( e ).class != Class }
				[ :@@before_filters, :@@after_filters, :@@json_validators ].each{ |variable| class_variable_get( variable ).clear }
			}
		}
		Dir[ Stairs.root.join 'app/actions/**/*.rb' ].each{ |file| load file }
	end

end
