FactoryGirl.define do
	factory :worker do |f|
		f.sequence( :email ){ |n| Faker::Internet.email.sub( '@', "#{ n }@" ) }
		f.password_digest{ Faker::Internet.password }
		f.name{ Faker::Internet.user_name }
		f.auth_token{ Faker::Crypto.md5 }
	end
end
