ENV[ 'environment' ] = 'test'

require_relative '../main'
Dir[ Stairs.root.join 'spec/support/**/*.rb' ].each{ |support| require_relative support }
Dir[ Stairs.root.join 'spec/factories/**/*.rb' ].each{ |factory| require_relative factory }

RSpec.configure do |config|
	config.include FactoryGirl::Syntax::Methods
	config.include ServerHelpers

	config.expect_with :rspec do |expectations|
		expectations.include_chain_clauses_in_custom_matcher_descriptions = true
	end

	config.mock_with :rspec do |mocks|
		mocks.verify_partial_doubles = true
	end

	if config.files_to_run.count <= 5
		config.default_formatter = 'doc'
	end

	config.profile_examples = 10

	config.before :suite do
		DatabaseCleaner.strategy = :transaction
		DatabaseCleaner.clean_with :truncation
	end

	config.before :each do
		clear_clients
		clear_actions
	end

	config.around :each do |example|
		instance = example.example_group

		if ( described_class = instance.described_class ) <= ::Action
			instance.subject{ described_class.instance }
		end

		DatabaseCleaner.cleaning do
			example.run
		end
	end
end
