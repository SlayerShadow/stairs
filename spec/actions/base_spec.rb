RSpec.describe Action::Base do
	describe '#authorize_customer' do
		let( :customer ){ create :customer }
		let( :worker ){ create :worker }

		it 'calls once' do
			expect( described_class.instance ).to receive( :authorize_customer ).once
			send_object service: 0, action: 0
		end

		context 'fails' do
			it 'without a token' do
				send_object service: 0, action: 0
				expect( read_object[ 'error' ] ).to eq( 'customer_not_found' )
			end

			it 'with wrong token' do
				send_object service: 0, action: 0, token: customer.auth_token + 'x'
				expect( read_object[ 'error' ] ).to eq( 'customer_not_found' )
			end

			context 'when worker' do
				it 'not sent' do
					send_object service: 0, action: 0, token: customer.auth_token
					expect( read_object[ 'error' ] ).to eq( 'worker_not_found' )
				end

				it 'not found' do
					send_object service: 0, action: 0, token: customer.auth_token, worker_id: worker.id + 1
					expect( read_object[ 'error' ] ).to eq( 'worker_not_found' )
				end

				it 'is busy' do
					allow( Action ).to receive( :worker_clients ).and_return( [ worker ] )
					send_object service: 0, action: 0, token: customer.auth_token, worker_id: worker.id
					expect( read_object[ 'error' ] ).to eq( 'worker_already_in_chat' )
				end
			end
		end

		context 'succeeds' do
			it 'with user and customer' do
				send_object service: 0, action: 0, token: customer.auth_token, worker_id: worker.id
				expect( read_object[ 'action' ] ).to eq( 0 )
				expect( Action.customer_clients.count ).to eq( 1 )
			end

			it 'with worker reference' do
				expect{
					send_object service: 0, action: 0, token: customer.auth_token, worker_id: worker.id
				}.to change( client, :worker ).from( nil ).to( worker )
			end

			it 'with clients connected increasing' do
				expect{
					send_object service: 0, action: 0, token: customer.auth_token, worker_id: worker.id
				}.to change( Action, :customer_clients ).from( [] ).to( [ client ] )
			end
		end
	end

	describe '#authorize_worker' do
		let( :customer ){ create :customer }
		let( :worker ){ create :worker }

		it 'calls once' do
			expect( described_class.instance ).to receive( :authorize_worker ).once
			send_object service: 0, action: 1
		end

		context 'fails' do
			it 'without a token' do
				send_object service: 0, action: 1
				expect( read_object[ 'error' ] ).to eq( 'worker_not_found' )
			end

			it 'with wrong token' do
				send_object service: 0, action: 1, token: worker.auth_token + 'x'
				expect( read_object[ 'error' ] ).to eq( 'worker_not_found' )
			end

			context 'when customer' do
				it 'not found' do
					send_object service: 0, action: 1, token: worker.auth_token, customer_id: customer.id + 1
					expect( read_object[ 'error' ] ).to eq( 'customer_not_connected' )
				end

				it 'connected to other worker' do
					other_client = build_client
					allow( Action ).to receive( :customer_clients ).and_return( [ other_client ] )

					other_client.id = customer.id
					other_client.worker = build :worker

					send_object service: 0, action: 1, token: worker.auth_token, customer_id: customer.id
					expect( read_object[ 'error' ] ).to eq( 'customer_reference_error' )
				end
			end
		end

		context 'succeeds' do
			context 'when customer' do
				it 'connected to same worker' do
					other_client = build_client
					allow( Action ).to receive( :customer_clients ).and_return( [ other_client ] )

					other_client.id = customer.id
					other_client.worker = worker

					send_object service: 0, action: 1, token: worker.auth_token, customer_id: customer.id
					expect( read_object ).to eq( 'service' => 0, 'action' => 0 )
				end
			end
		end
	end
end
