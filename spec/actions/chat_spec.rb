RSpec.describe Action::Chat do
	describe '#received_message' do
		context 'filtered by' do
			it '#check_outside_client' do
				expect( described_class.instance ).to receive( :check_outside_client ).and_call_original
				expect( described_class.instance ).not_to receive( :check_message )
				expect( described_class.instance ).not_to receive( :received_message )
				send_object service: 1, action: 0
			end

			it '#check_message' do
				allow( client ).to receive( :online_client ).and_return( build_client )

				expect( described_class.instance ).to receive( :check_outside_client ).and_wrap_original{|method, *args|
					result = method.call *args
					expect( result ).to be_truthy
					result
				}
				expect( described_class.instance ).to receive( :check_message ).and_wrap_original{|method, *args|
					result = method.call *args
					expect( result ).to be_falsy
					result
				}
				expect( described_class.instance ).not_to receive( :received_message )

				send_object service: 1, action: 0
				expect( read_object[ 'error' ] ).to eq( 'no_message' )
			end
		end

		context 'fails' do
			it 'with invalid user' do
				expect( client ).to receive( :close_connection )
				send_object service: 1, action: 0, message: 'test'
				expect( read_object[ 'error' ] ).to eq( 'auth_required' )
			end
		end

		context 'succeeds' do
			it 'when connected to other client' do
				other_client = build_client
				message = 'Hello, world!'

				client.online_client = other_client
				send_object service: 1, action: 0, message: message

				expect( read_object ).to eq( 'service' => 1, 'action' => 0 )
				expect( read_object other_client ).to eq( 'service' => 1, 'action' => 1, 'message' => message )
			end
		end
	end
end
