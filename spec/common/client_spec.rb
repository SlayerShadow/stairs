RSpec.describe ApplicationName::Client do
	let( :customer ){ create :customer }
	let( :worker ){ create :worker }

	describe 'raises error' do
		it 'when IO is empty' do
			send_object service: 0, action: 0
			expect( read_object ).to be_present
			expect{ read_object }.to raise_error( IOError )
		end

		it 'when nothing sent' do
			expect{ read_object }.to raise_error( IOError )
		end
	end

	describe 'starts messaging' do
		it 'with full valid algorythm' do
			send_object service: 0, action: 0, token: customer.auth_token, worker_id: worker.id
			expect( read_object ).to eq( 'service' => 0, 'action' => 0  )

			other_client = build_client
			send_object other_client, service: 0, action: 1, token: worker.auth_token, customer_id: customer.id
			expect( read_object other_client ).to eq( 'service' => 0, 'action' => 0  )

			message = 'test'
			send_object service: 1, action: 0, message: message
			expect( read_object ).to eq( 'service' => 1, 'action' => 0 )
			expect( read_object( other_client )[ 'message' ] ).to eq( message )

			message = 'test2'
			send_object other_client, service: 1, action: 0, message: message
			expect( read_object other_client ).to eq( 'service' => 1, 'action' => 0 )
			expect( read_object[ 'message' ] ).to eq( message )
		end
	end

	describe 'disconnection' do
		before do
			send_object service: 0, action: 0, token: customer.auth_token, worker_id: worker.id
		end

		it 'calls #unbind' do
			expect( client ).to receive( :unbind )
			client.close_connection
		end

		it 'removes client from #customer_clients' do
			expect{ client.close_connection }.to change( Action, :customer_clients ).from( [ client ] ).to( [] )
		end
	end
end
