class Action
	@@worker_clients, @@customer_clients = [], []

	def self.worker_clients
		@@worker_clients
	end

	def self.customer_clients
		@@customer_clients
	end
end
