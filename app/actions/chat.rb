class Action::Chat < Action
	before_filter :check_outside_client
	before_filter :check_message

	def received_message client, data
		logger.debug message_received: data
		client.online_client.message :chat, :message, data
		client.message :chat, :success
	end

	private

	def check_outside_client client, data
		unless client.online_client
			client.message :system, :failed, error: 'auth_required'
			client.close_connection
		end
		client.online_client
	end

	def check_message client, data
		client.message :system, :failed, error: 'no_message' unless data[ 'message' ]
		data[ 'message' ]
	end
end
