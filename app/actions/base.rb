require Stairs.root.join( 'lib/client_helper' )

class Action::Base < Action
	def authorize_customer client, data
		logger.debug authorize_customer: data

		if user = Customer.find_by_auth_token( data[ 'token' ] )
			client.id = user.id
			client.unbind_callback method( :unbind )
			Action.customer_clients << client

			if Action.worker_clients.find{ |worker| worker.id == data[ 'worker_id' ] }
				client.message :system, :failed, error: :worker_already_in_chat
				client.close_connection
			else
				client.worker = Worker.find_by_id data[ 'worker_id' ]
				if client.worker
					client.message :system, :success
				else
					client.message :system, :failed, error: :worker_not_found
					client.close_connection
				end
			end
		else
			client.message :system, :failed, error: :customer_not_found
			client.close_connection
		end
	end

	def authorize_worker client, data
		logger.debug authorize_worker: data

		if user = Worker.find_by_auth_token( data[ 'token' ] )
			client.id = user.id
			client.unbind_callback method( :unbind )
			Action.worker_clients << client

			if client.online_client = Action.customer_clients.find{ |customer| customer.id == data[ 'customer_id' ] }
				if client.online_client.worker != user
					client.message :system, :failed, error: :customer_reference_error
					client.close_connection
				else
					client.online_client.online_client = client
					client.message :system, :success
				end
			else
				client.message :system, :failed, error: :customer_not_connected
				client.close_connection
			end
		else
			client.message :system, :failed, error: :worker_not_found
			client.close_connection
		end
	end

	private

	def unbind client
		client.worker = nil
		client.online_client.message :system, :close if client.online_client
		Action.customer_clients.delete client
		Action.worker_clients.delete client
	end
end
